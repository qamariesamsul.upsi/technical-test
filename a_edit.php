<?php
require('db.php');
error_reporting(~E_NOTICE);
DATE_DEFAULT_TIMEZONE_SET('Asia/Kuala_lumpur');
if (isset($_POST['update'])) {
    $name = strip_tags($_POST['mname']);
    $email = strip_tags($_POST['email']);
    //$password = strip_tags($_POST['password']);
    $name = filter_var($name, FILTER_SANITIZE_STRING);
    $email = filter_var($email, FILTER_VALIDATE_EMAIL);
    $gender = strip_tags($_POST['gender']);
    $phone_no = strip_tags($_POST['phone_no']);
    $address = strip_tags($_POST['address']);
    $gender = filter_var($gender, FILTER_SANITIZE_STRING);
    $phone_no = filter_var($phone_no, FILTER_SANITIZE_STRING);
    $adress = filter_var($address, FILTER_SANITIZE_STRING);
    $time = date('y-m-d h:i:s', time());
    $date = date('y-m-d', strtotime('now'));
    if (empty($name)) {
        $_SESSION['msg'] = "Name is Required";
    } elseif (!filter_var($name, FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => "/^[a-zA-Z\s]+$/")))) {
        $_SESSION['msg'] = "Name contain invalid characters";
    } elseif (empty($email)) {
        $_SESSION['msg'] = "email required";
    } elseif (empty($password)) {
        $_SESSION['msg'] = "password required";
    } elseif (empty($gender)) {
        $_SESSION['msg'] = "gender required";
    } elseif (!isset($_SESSION['msg'])) {
        $encrypt = password_hash($password, PASSWORD_DEFAULT);
        $sth = $db->prepare("UPDATE members SET mname='$name', email='$email', gender='$gender', phone_no='$phone_no', address='$address' WHERE user_id='$_GET[mem_id]' ");
        /*$sth->bindparam(1, $name, PDO::PARAM_STR);
        $sth->bindparam(2, $email, PDO::PARAM_STR);
        $sth->bindparam(3, $encrypt, PDO::PARAM_STR);
        $sth->bindparam(4, $gender, PDO::PARAM_STR);
        $sth->bindparam(7, $time);
        $sth->bindparam(8, $date);*/
        $sth->execute();
        header("location:index.php");
    }
}
?>

<html>

<head>
    <title>Edit User Information</title>
</head>

<body>
    <table width="70%">
        <tr>
            <th>Name: </th><td><input type="text" size="30px" value=""></td>
            <th>Email: </th><td><input type="email" size="50px" value=""></td>
            <th>Gender: </th><td><input type="text" size="15px" value=""></td>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </table>
</body>

</html>