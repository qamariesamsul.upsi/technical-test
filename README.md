# Technical Test

requirement for execute localhost
- xampp (apache and mysql **service started**)


1. Open **phpmyadmin** to import .sql file from _db folder._
    - **uncheck** Enables foreign key checks
    - chose coresponding _.sql file_.
    - click button **go**


2. Open web browser (chrome, firefox, Microsoft Edge).
    - type-in at the address bar (localhost/[name or path to project folder])


# User

Privilege (admin)
**username**: admin | **pwd**: 123456

Privilege (user)
**username**: Qamarie Samsul | **pwd**: 123456
